$(document).ready(function () {
    (function pageLoad() {
        $mz.onload();
        $mz.cfg.validation.color.invalid.background.hex = "#ffe53b";
    })();

    var flag = false;

    var show = {
        forward: {
            p: {opacity: [1, 0], translateX: [0, "100%"]},
            // p: {opacity: [ 1, 0 ],translateZ: 0},
            o: {duration: 250, display: 'flex'}
        },
        back: {
            p: {opacity: [1, 0], translateX: [0, "-100%"]},
            // p: {opacity: [ 1, 0 ],translateZ: 0},
            o: {duration: 250, display: 'flex'}
        }
    };

    var hide = {
        forward: {
            p: {opacity: [0, 1], translateX: ["-100%", 0]},
            // p: {opacity: [ 0, 1 ],translateZ: 0},
            o: {duration: 250, display: 'none'}
        },
        back: {
            p: {opacity: [0, 1], translateX: ["100%", 0]},
            // p: {opacity: [ 0, 1 ],translateZ: 0},
            o: {duration: 250, display: 'none'}
        }
    };

    function showHideView(objHide, objShow, direction) {
        window.scrollTo(0, 0);
        if (!flag) {
            flag = true;

            if (direction === 'forward') {
                objHide.velocity(hide.forward.p, hide.forward.o);
                objShow.velocity(show.forward.p, show.forward.o);
            }

            if (direction === 'back') {
                objHide.velocity(hide.back.p, hide.back.o);
                objShow.velocity(show.back.p, show.back.o);
            }

            var flagTimer = setTimeout(function () {
                clearTimeout(flagTimer);
                flag = false;
            }, 600)
        }
    };



    function highlightEmailDom() {
        var bgColor = $('#Email').css('background-color');
        $('#EmailDomain').css('background-color', bgColor);
    }

// Validate MANDATORY/OPTIONAL Input field behaviour
    function ReportResult() {

        //mandatory/optional
        // var mobile = $('#Mobile');
        // var email = $('#Email');
        // var mobileOptional, emailOptional = 'false';
        // if(!mobile.val() && email.val()){
        //     mobileOptional = 'true';
        // }else if(mobile.val() && !email.val()){
        //     emailOptional = 'true';
        // }else{
        //     mobileOptional = 'false';
        //     emailOptional = 'false';
        // }
        // mobile.attr('data-optional', mobileOptional);
        // email.attr('data-optional', emailOptional);
        //END mandatory/optional

        if ($mz.validate.onsubmit("mozeus")) {
            SubmitResult(this.document, 'mozeus');

            //LINES BELOW PART OF SPA LOGIC
            showHideView($('#data'), $('#finish'), 'forward');
            Finish();
        }

        if ($mz.validate.email) {
            console.log('valid');
        }

        highlightEmailDom();
    }

    $('#Email').keydown(function () {
        setTimeout(function () {
            highlightEmailDom();
        }, 200);
    });


    $('#start').on('touchstart click', function (e) {
        e.stopPropagation();
        e.preventDefault();
        showHideView($('#start'), $('#data'), 'forward');
    });

    $('#submit').on('touchstart click', function (e) {
        e.stopPropagation();
        e.preventDefault();

        $('#submit').addClass('push');
        $('#submit').one('webkitAnimationEnd animationend', function () {
            $('#submit').removeClass('push');
        });
        ReportResult();
    });
});


// INPUT TEXT LENGTH LISTENER TO APPLY SPAN ANIMATIONS
var textInput = $(".input-wrap input");
var spanPlaceholder = $(".input-placeholder");

textInput.on('keyup change', function(e) {
    e.preventDefault();
    e.stopPropagation();
    console.log($(this).val().length);

    spanPlaceholder.addClass("minified");

    if (textInput.val().length == 0) {
        spanPlaceholder.removeClass("minified");
    }
});




/*========== TIMEOUT PAGES TO EXIT HTML ==========*/
//     var timer = setTimeout(function () {
//         Finish();
//     }, 5000);
//
//     function Finish() {
//         clearTimeout(timer);
//         Exit();
//     }





/*========== *ALTERNATE SYNTAX FOR MANUALLY BUILDING RESULT STRINGS ==========*/
// var data = BuildResult(this.document, 'mozeus');
// SubmitResultString(data);





/*========== ALTERNATE MANUAL VALIDATION ==========*/
// function validateFormOnSubmit() {
//     var validDate = $mz.validate.date.dropdowns($mz.find("mz_BirthMonth"), $mz.find("mz_BirthDay"), $mz.find("mz_BirthYear"));
//
//     var fName = $mz.validate.element($mz.find("FName"));
//     var lName = $mz.validate.element($mz.find("LName"));
//     var email = $mz.validate.element($mz.find("Email"));
//     var zip = $mz.validate.element($mz.find("Zip"));
//
//     return fName && lName && email && zip;
// }





/*========== FUNCTION TO VALIDATE CHECKBOXES WITHIN WRAPPER ID ==========*/
// function valOptIn(id) {
//     var getOptIn = document.getElementById(id);
//     var info = getOptIn.getElementsByTagName('input');
//     var i;
//     for (i = 0; i < info.length; ++ i){
//         if (info[i].checked) {
//             $('#'+id).removeClass('invalid');
//             return true;
//         }
//     }//loop group
//     $('#'+id).addClass('invalid');
//
//     return false;
// }





/*========== FUNCTION TO BUILD RETURN STRINGS FOR CHECKBOXES ==========*/
// function buildChecks(which, checkID){
//     var gatherMethod = '';
//     var counter = 0;
//     var getBox = document.getElementById(which);
//     var getInputsBox = getBox.getElementsByTagName('input');
//
//     for (var i = 0; i < getInputsBox.length; i++) {
//
//         if (getInputsBox[i].type == 'checkbox' && getInputsBox[i].checked) {
//             counter++;
//             gatherMethod += checkID + counter + "=" + getInputsBox[i].value + ';';
//         }
//     }
//     return gatherMethod;
// }





/*========== DOB VALIDATION FOR INPUT ==========*/
// function isvalid_mdy(s) {
//
//     try {
//
//         var day, A = s.match(/[1-9][\d]*/g);
//
//         A[0] -= 1;
//         day = new Date(+A[2], A[0], +A[1]);
//         if (day.getMonth() == A[0] && day.getDate() == A[1]) {
//
//             return true;
//
//         } else {
//             console.log('Bad Date');
//             $('#DOB').css('background-color', '#ffe53b');
//             return false;
//         }
//
//     } catch (er) {
//         return false;
//     }
//
// }
//
// function verifyAge(dob) {
//     var today = new Date();
//     var birthDate = new Date(dob);
//     var age = today.getFullYear() - birthDate.getFullYear();
//     var m = today.getMonth() - birthDate.getMonth();
//     if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
//         age--;
//     }
//
//     console.log(age);
//     return age;
// }
//
// function valAge(dob) {
//
//     var age = verifyAge(dob);
//
//     if (isNaN(age) || age <= 0 || age > 120) {
//         return false;
//     } else {
//         return true;
//     }
//
// }





/*========== Swap Not Exported VOI ==========*/
// var model = "";
// var voi = "";
// var mod1 = $('#mz_ModelInterest1');
// var mod2 = $('#mz_ModelInterest2');
// var mod3 = $('#mz_ModelInterest3');

// function moveUpNoExportVoi() {
//
//     if (mod1.val() === "VEHICLENAME HERE -- IMPORTANT") {
//         if (mod2.val()) {
//             voi += "ModelInterest1=" + mod2.val() + ";ModelInterest2=" + mod1.val() + ";"
//
//             if (mod3.val()) {
//                 voi += "ModelInterest3=" + mod3.val() + ";"
//             }
//
//         } else {
//             voi += "ModelInterest1=" + mod1.val() + ";"
//         }
//     } else {
//
//         if (mod1.val()) {
//             voi += "ModelInterest1=" + mod1.val() + ";"
//         }
//
//         if (mod2.val()) {
//             voi += "ModelInterest2=" + mod2.val() + ";"
//         }
//
//         if (mod3.val()) {
//             voi += "ModelInterest3=" + mod3.val() + ";"
//         }
//     }
//
//     console.log(voi);
//     console.log('fired');
//
//     if (mod2.val() == "" || mod2.val() == null) {
//         model = "ModelInterest2=MZ-IGNORE-RESULT-MZ;ModelInterest3=MZ-IGNORE-RESULT-MZ;";
//     } else if (mod2.val() !== "" && (mod3.val() == "" || mod3.val() == null)) {
//         model = "ModelInterest3=MZ-IGNORE-RESULT-MZ;";
//     } else {
//         model = "";
//     }
// }





/*========== DL SCAN INPUT Transofrm for Selection DROPDOWNS ==========*/
// var count = 0;
//
// var dateCheck = setTimeout(function(e){
//     clearTimeout(dateCheck);
//     checkExpDate();
// }, 200);
//
// function checkExpDate(){
//     expVal = $('#ExpirationDate').val();
//
//     console.log('checking!');
//     console.log(count)
//
//     if(count < 10){
//         if(expVal){
//             var month = expVal.substring(0, 2);
//             var day = expVal.substring(2, 4);
//             var year = expVal.substring(4, 8);
//
//             $('#mz_Month').val(month);
//             $('#mz_Day').val(day);
//             $('#mz_Year').val(year);
//         }else{
//             count++;
//             var dateCheck = setTimeout(function(e) {
//                 clearTimeout(dateCheck);
//                 checkExpDate();
//             }, 1000);
//         }
//     }
// }