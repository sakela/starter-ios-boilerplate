/**
 *
 * https://mozeus.atlassian.net/wiki/display/GBT/Gulp+Build+Tools+Home
 *
 * gulp init - Create starter kits for all platforms
 *
 * Create starter kits individually for each platform
 * gulp init-ios
 * gulp init-android
 * gulp init-windows
 * gulp init-jumpstart
 * gulp init-photo-reel
 * gulp init-video-reel
 * gulp init-leaderboard
 *
 * builds will automatically include OS.js, MZ.js, jQuery, animate.css, velocity.js, and velocity.ui.js
 * **/

/**
 * Dependencies.
 */
var gulp = require('gulp');
var download = require("gulp-download");
var template = require('gulp-template-html');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var http = require('http');
var ecstatic = require('ecstatic');
var clean = require('gulp-clean');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var replace = require('gulp-replace');
var flatten = require('gulp-flatten');
var rename = require("gulp-rename");
var plumber = require('gulp-plumber');
var inlineCSS = require('gulp-inline-css');
var file = require('gulp-file');
var source = require('vinyl-source-stream');
var runSequence = require('run-sequence');
var imagemin = require('gulp-imagemin');
var browserSync = require('browser-sync').create();
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var fileExists = require('file-exists');


// delay directory creation to ensure that .gitkeep exists in the root directory
var delay = 5000;

// Define some file paths.
var paths_ios = {
    pages: ['Project/Assets/Devices/iOS/**/*.html'],
    images: ['Project/Assets/Devices/iOS/img/*.{jpg,gif,png,svg}'],
    sass: ['Project/Assets/Devices/iOS/sass/**/*.scss'],
    css: ['Project/Assets/Devices/iOS/css/**']
};

var paths_android = {
    pages: ['Project/Assets/Devices/Android/**/*.html'],
    images: ['Project/Assets/Devices/Android/img/*.{jpg,gif,png,svg}'],
    sass: ['Project/Assets/Devices/Android/sass/**/*.scss'],
    css: ['Project/Assets/Devices/Android/css/**']
};

var paths_windows = {
    pages: ['Project/Assets/Devices/Windows/**/*.html'],
    images: ['Project/Assets/Devices/Windows/img/*.{jpg,gif,png,svg}'],
    sass: ['Project/Assets/Devices/Windows/sass/**/*.scss'],
    css: ['Project/Assets/Devices/Windows/css/**']
};

var paths_jumpstart = {
    pages: ['Project/Assets/Jumpstart/**/*.html'],
    images: ['Project/Assets/Jumpstart/img/*.{jpg,gif,png,svg}'],
    sass: ['Project/Assets/Jumpstart/sass/**/*.scss'],
    css: ['Project/Assets/Jumpstart/css/**']
};

var paths_leaderboard = {
    pages: ['Project/Assets/Media Reel/Leaderboard/**/*.html'],
    images: ['Project/Assets/Media Reel/Leaderboard/img/*.{jpg,gif,png,svg}'],
    sass: ['Project/Assets/Media Reel/Leaderboard/sass/**/*.scss'],
    css: ['Project/Assets/Media Reel/Leaderboard/css/**']
};

var paths_photoReel = {
    pages: ['Project/Assets/Media Reel/Photo Reel/**/*.html'],
    images: ['Project/Assets/Media Reel/Photo Reel/img/*.{jpg,gif,png,svg}'],
    sass: ['Project/Assets/Media Reel/Photo Reel/sass/**/*.scss'],
    css: ['Project/Assets/Media Reel/Photo Reel/css/**']
};

var paths_videoReel = {
    pages: ['Project/Assets/Media Reel/Video Reel/**/*.html'],
    images: ['Project/Assets/Media Reel/Video Reel/img/*.{jpg,gif,png,svg}'],
    sass: ['Project/Assets/Media Reel/Video Reel/sass/**/*.scss'],
    css: ['Project/Assets/Media Reel/Video Reel/css/**']
};


var rootDir = {
    ios: 'Project/Assets/Devices/iOS',
    android: 'Project/Assets/Devices/android',
    windows: 'Project/Assets/Devices/windows',
    jumpstart: 'Project/Assets/Jumpstart',
    leaderboard: 'Project/Assets/Media Reel/Leaderboard',
    photoReel: 'Project/Assets/Media Reel/Photo Reel',
    videoReel: 'Project/Assets/Media Reel/Video Reel'
};

var scriptFiles = {
    ios: [
        rootDir.ios + '/js/**/*.js',
        '!' + rootDir.ios + '/js/app-ios.js',
        '!' + rootDir.ios + '/js/mz/**',
        '!' + rootDir.ios + '/js/vendor/**'
    ],

    android: [
        rootDir.android + '/js/**/*.js',
        '!' + rootDir.android + '/js/app-android.js',
        '!' + rootDir.android + '/js/mz/**',
        '!' + rootDir.android + '/js/vendor/**'
    ],

    windows: [
        rootDir.windows + '/js/**/*.js',
        '!' + rootDir.windows + '/js/mzWinSA.js',
        '!' + rootDir.windows + '/js/mz/**',
        '!' + rootDir.windows + '/js/vendor/**'
    ],

    jumpstart: [
        rootDir.jumpstart + '/js/**/*.js',
        '!' + rootDir.jumpstart + '/js/app-jumpstart.js',
        '!' + rootDir.jumpstart + '/js/mz/**',
        '!' + rootDir.jumpstart + '/js/vendor/**'
    ],

    leaderboard: [
        rootDir.leaderboard + '/js/**/*.js',
        '!' + rootDir.leaderboard + '/js/app-leaderboard.js',
        '!' + rootDir.leaderboard + '/js/mz/**',
        '!' + rootDir.leaderboard + '/js/vendor/**'
    ],

    photoReel: [
        rootDir.photoReel + '/js/**/*.js',
        '!' + rootDir.photoReel + '/js/app-photoreel.js',
        '!' + rootDir.photoReel + '/js/mz/**',
        '!' + rootDir.photoReel + '/js/vendor/**'
    ],

    videoReel: [
        rootDir.videoReel + '/js/**/*.js',
        '!' + rootDir.videoReel + '/js/app-videoreel.js',
        '!' + rootDir.videoReel + '/js/mz/**',
        '!' + rootDir.videoReel + '/js/vendor/**'
    ]
};


//ALL
gulp.task('init', ['init-ios', 'init-android', 'init-windows', 'init-jumpstart', 'init-photo-reel', 'init-video-reel', 'init-leaderboard'], function () {
    // return gulp.src([
    //     '.gitkeep'
    // ])
    //
    //     .pipe(gulp.dest('Project/Assets'))

});


//iOS
var createDirIos = function () {

    console.log('TWO****************************');

    return gulp.src([
        '.gitkeep'
    ])

    //Creative
        .pipe(gulp.dest('Project/Creative'))
        .pipe(gulp.dest('Project/Creative/Mocks'))
        .pipe(gulp.dest('Project/Creative/Source Files'))
        //Email
        .pipe(gulp.dest('Project/Assets/Emails'))
        //Overlays
        .pipe(gulp.dest('Project/Assets/Overlays'))
        //IOS
        .pipe(gulp.dest('Project/Assets/Devices'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/css'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/fonts'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/img'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/mz'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/sass'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/build'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/static'))
        .pipe(gulp.dest('Project/Assets/Devices/iOS/survey'))

};

gulp.task('init-ios', function () {

    //check to see if ios directory exists by checking for a .gitkeep file
    fileExists('Project/Assets/Devices/iOS/.gitkeep', function (err, exists) {

        if (!exists) {
            runSequence('gitkeep',
                ['add-lib-ios', 'add-html-temp-ios', 'add-files-ios'], 'create-dir-ios');
        }

    })

});

gulp.task('create-dir-ios', function () {

    setTimeout(createDirIos, delay);

});


//Android
var createDirAndroid = function () {

    return gulp.src([
        '.gitkeep'
    ])

    //Creative
        .pipe(gulp.dest('Project/Creative'))
        .pipe(gulp.dest('Project/Creative/Mocks'))
        .pipe(gulp.dest('Project/Creative/Source Files'))
        //Email
        .pipe(gulp.dest('Project/Assets/Emails'))
        //Overlay
        .pipe(gulp.dest('Project/Assets/Overlay'))
        //ANDROID
        .pipe(gulp.dest('Project/Assets/Devices/Android'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/css'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/fonts'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/img'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/js'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/mz'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/sass'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/build'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/static'))
        .pipe(gulp.dest('Project/Assets/Devices/Android/survey'))
};

gulp.task('init-android', function () {

    //check to see if android directory exists by checking for a .gitkeep file
    fileExists('Project/Assets/Devices/Android/.gitkeep', function (err, exists) {

        if (!exists) {

            runSequence('gitkeep',
                ['add-lib-android', 'add-html-temp-android', 'add-files-android'], 'create-dir-android');

        }

    })

});

gulp.task('create-dir-android', function () {

    setTimeout(createDirAndroid, delay);

});


//WINDOWS
var createDirWindows = function () {

    return gulp.src([
        '.gitkeep'
    ])

    //Creative
        .pipe(gulp.dest('Project/Creative'))
        .pipe(gulp.dest('Project/Creative/Mocks'))
        .pipe(gulp.dest('Project/Creative/Source Files'))
        //Email
        .pipe(gulp.dest('Project/Assets/Emails'))
        //Overlay
        .pipe(gulp.dest('Project/Assets/Overlay'))
        //WINDOWS
        .pipe(gulp.dest('Project/Assets/Devices/Windows'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/css'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/fonts'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/img'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/mz'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/sass'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/build'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/static'))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/survey'))

};


gulp.task('init-windows', function () {


    //check to see if windows directory exists by checking for a .gitkeep file
    fileExists('Project/Assets/Devices/Windows/.gitkeep', function (err, exists) {
        "use strict";

        if (!exists) {
            runSequence('gitkeep',
                ['add-lib-win', 'add-html-temp-windows', 'add-files-windows'], 'create-dir-windows');
        }

    })

});

gulp.task('create-dir-windows', function () {

    setTimeout(createDirWindows, delay);

});


//JUMPSTART
var createDirJumpstart = function () {

    return gulp.src([
        '.gitkeep'
    ])
    //Creative
        .pipe(gulp.dest('Project/Creative'))
        .pipe(gulp.dest('Project/Creative/Mocks'))
        .pipe(gulp.dest('Project/Creative/Source Files'))
        //Email
        .pipe(gulp.dest('Project/Assets/Emails'))
        //Overlay
        .pipe(gulp.dest('Project/Assets/Overlay'))
        //Jumpstart
        .pipe(gulp.dest('Project/Assets/Jumpstart'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/css'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/fonts'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/img'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/js'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/js/vendor'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/js/mz'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/sass'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/build'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/static'))
        .pipe(gulp.dest('Project/Assets/Jumpstart/survey'))
};


gulp.task('init-jumpstart', function () {

    //check to see if jumpstart directory exists by checking for a .gitkeep file
    fileExists('Project/Assets/Jumpstart/.gitkeep', function (err, exists) {

        if (!exists) {

            runSequence('gitkeep',
                ['add-lib-jumpstart', 'add-html-temp-web', 'add-files-jumpstart'], 'create-dir-jumpstart');

        }

    })

});


gulp.task('create-dir-jumpstart', function () {

    setTimeout(createDirJumpstart, delay);

});


//PHOTO REEL
var createDirPhotoReel = function () {

    return gulp.src([
        '.gitkeep'
    ])

        .pipe(gulp.dest('Project/Creative'))
        .pipe(gulp.dest('Project/Creative/Mocks'))
        .pipe(gulp.dest('Project/Creative/Source Files'))
        //Email
        .pipe(gulp.dest('Project/Assets/Emails'))
        //Overlay
        .pipe(gulp.dest('Project/Assets/Overlay'))
        .pipe(gulp.dest('Project/Assets/Media Reel'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel/css'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel/fonts'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel/img'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel/js'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel/sass'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel/build'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel/static'))
};

gulp.task('init-photo-reel', function () {

    //check to see if photo reel directory exists by checking for a .gitkeep file
    fileExists('Project/Assets/Media Reel/Photo Reel/.gitkeep', function (err, exists) {

        if (!exists) {

            // runSequence('gitkeep', 'create-dir-photo-reel');
            runSequence('gitkeep', ['add-html-temp-photoReel', 'add-files-photoReel'], 'create-dir-photo-reel');

        }

    })

});

gulp.task('create-dir-photo-reel', function () {

    setTimeout(createDirPhotoReel, delay);

});


//VIDEO REEL
var createDirVideoReel = function () {

    return gulp.src([
        '.gitkeep'
    ])

        .pipe(gulp.dest('Project/Creative'))
        .pipe(gulp.dest('Project/Creative/Mocks'))
        .pipe(gulp.dest('Project/Creative/Source Files'))
        //Email
        .pipe(gulp.dest('Project/Assets/Emails'))
        //Overlay
        .pipe(gulp.dest('Project/Assets/Overlay'))
        .pipe(gulp.dest('Project/Assets/Media Reel'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel/css'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel/fonts'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel/img'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel/js'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel/sass'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel/build'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel/static'))
};

gulp.task('init-video-reel', function () {

    //check to see if video reel directory exists by checking for a .gitkeep file
    fileExists('Project/Assets/Media Reel/Video Reel/.gitkeep', function (err, exists) {

        if (!exists) {

            // runSequence('gitkeep', 'create-dir-video-reel');
            runSequence('gitkeep', ['add-html-temp-videoReel', 'add-files-videoReel'], 'create-dir-video-reel');

        }

    })

});

gulp.task('create-dir-video-reel', function () {

    setTimeout(createDirVideoReel, delay);

});


//LEADERBOARD
var createDirLeaderboard = function () {

    return gulp.src([
        '.gitkeep'
    ])
        .pipe(gulp.dest('Project/Creative'))
        .pipe(gulp.dest('Project/Creative/Mocks'))
        .pipe(gulp.dest('Project/Creative/Source Files'))
        //Email
        .pipe(gulp.dest('Project/Assets/Emails'))
        //Overlay
        .pipe(gulp.dest('Project/Assets/Overlay'))
        .pipe(gulp.dest('Project/Assets/Media Reel'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard/css'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard/fonts'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard/img'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard/js'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard/sass'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard/build'))
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard/static'))
};


gulp.task('init-leaderboard', function () {

    //check to see if video reel directory exists by checking for a .gitkeep file
    fileExists('Project/Assets/Media Reel/Leaderboard/.gitkeep', function (err, exists) {

        if (!exists) {

            runSequence('gitkeep', ['add-html-temp-leaderboard', 'add-files-leaderboard'], 'create-dir-leaderboard');

        }

    })

});


gulp.task('create-dir-leaderboard', function () {

    setTimeout(createDirLeaderboard, delay);

});


gulp.task('gitkeep', function () {

    var f;

    try {
        f = require('your-file');
    } catch (error) {

        createFile('.gitkeep', '', './');
        console.log(f);
    }

    if (f) {
        console.log(f);
    }

});


//Get ios template from web and place in ios directory
gulp.task('add-html-temp-ios', function () {

    download('https://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/index.html')
        .pipe(gulp.dest('Project/Assets/Devices/iOS'));

});

//Get windows template from web and place in windows directory
gulp.task('add-html-temp-android', function () {

    download('https://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/index.html')
        .pipe(gulp.dest('Project/Assets/Devices/android'));

});

//Get android template from web and place in android directory
gulp.task('add-html-temp-windows', function () {

    download('https://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/index.html')
        .pipe(gulp.dest('Project/Assets/Devices/windows'));

});

//Get jumpstart template from web and place in web directory
gulp.task('add-html-temp-web', function () {

    download('https://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/web/index.html')
        .pipe(gulp.dest('Project/Assets/Jumpstart'));

});


//Get leaderboard template from web and place in leaderboard directory
gulp.task('add-html-temp-leaderboard', function () {

    download('https://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/web/index.html')
        .pipe(gulp.dest('Project/Assets/Media Reel/Leaderboard'));

});


//Get photo reel template from web and place in Photo Reel directory
gulp.task('add-html-temp-photoReel', function () {

    download('https://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/web/index.html')
        .pipe(gulp.dest('Project/Assets/Media Reel/Photo Reel'));

});


//Get video reel template from web and place in Video Reel directory
gulp.task('add-html-temp-videoReel', function () {

    download('https://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/web/index.html')
        .pipe(gulp.dest('Project/Assets/Media Reel/Video Reel'));

});


// create js and css files needed in the ios starter kit
gulp.task('add-files-ios', function () {

    createFile('app-ios.css', '/** * write some awesome css here */', 'Project/Assets/Devices/iOS/css');
    createFile('app-ios.scss', '/** * write some awesome scss here */', 'Project/Assets/Devices/iOS/sass');
    createFile('app-ios.js', '/** * write some awesome js here */', 'Project/Assets/Devices/iOS/js');
    createFile('main.js', '/** * write some awesome js here */', 'Project/Assets/Devices/iOS/js');

});


// create js and css files needed in the android starter kit
gulp.task('add-files-android', function () {

    createFile('app-android.css', '/** * write some awesome css here */', 'Project/Assets/Devices/Android/css');
    createFile('app-android.scss', '/** * write some awesome scss here */', 'Project/Assets/Devices/Android/sass');
    createFile('app-android.js', '/** * write some awesome js here */', 'Project/Assets/Devices/Android/js');
    createFile('main.js', '/** * write some awesome js here */', 'Project/Assets/Devices/Android/js');

});


// create js and css files needed in the windows starter kit
gulp.task('add-files-windows', function () {

    createFile('mzWinSA.css', '/** * write some awesome css here */', 'Project/Assets/Devices/Windows/css');
    createFile('mzWinSA.scss', '/** * write some awesome scss here */', 'Project/Assets/Devices/Windows/sass');
    createFile('mzWinSA.js', '/** * write some awesome js here */', 'Project/Assets/Devices/Windows/js');
    createFile('main.js', '/** * write some awesome js here */', 'Project/Assets/Devices/Windows/js');

});


// create js and css files needed in the Jumpstart starter kit
gulp.task('add-files-jumpstart', function () {

    createFile('app-jumpstart.css', '/** * write some awesome css here */', 'Project/Assets/Jumpstart/css');
    createFile('app-jumpstart.scss', '/** * write some awesome scss here */', 'Project/Assets/Jumpstart/sass');
    createFile('app-jumpstart.js', '/** * write some awesome js here */', 'Project/Assets/Jumpstart/js');
    createFile('main.js', '/** * write some awesome js here */', 'Project/Assets/Jumpstart/js');

});


// create js and css files needed in the leaderboard starter kit
gulp.task('add-files-leaderboard', function () {

    createFile('app-leaderboard.css', '/** * write some awesome css here */', 'Project/Assets/Media Reel/Leaderboard/css');
    createFile('app-leaderboard.scss', '/** * write some awesome scss here */', 'Project/Assets/Media Reel/Leaderboard/sass');
    createFile('app-leaderboard.js', '/** * write some awesome js here */', 'Project/Assets/Media Reel/Leaderboard/js');
    createFile('main.js', '/** * write some awesome js here */', 'Project/Assets/Media Reel/Leaderboard/js');

});


// create js and css files needed in the photo reel starter kit
gulp.task('add-files-photoReel', function () {

    createFile('app-photoreel.css', '/** * write some awesome css here */', 'Project/Assets/Media Reel/Photo Reel/css');
    createFile('app-photoreel.scss', '/** * write some awesome scss here */', 'Project/Assets/Media Reel/Photo Reel/sass');
    createFile('app-photoreel.js', '/** * write some awesome js here */', 'Project/Assets/Media Reel/Photo Reel/js');
    createFile('main.js', '/** * write some awesome js here */', 'Project/Assets/Media Reel/Photo Reel/js');

});

// create js and css files needed in the video reel starter kit
gulp.task('add-files-videoReel', function () {

    createFile('app-videoreel.css', '/** * write some awesome css here */', 'Project/Assets/Media Reel/Video Reel/css');
    createFile('app-videoreel.scss', '/** * write some awesome scss here */', 'Project/Assets/Media Reel/Video Reel/sass');
    createFile('app-videoreel.js', '/** * write some awesome js here */', 'Project/Assets/Media Reel/Video Reel/js');
    createFile('main.js', '/** * write some awesome js here */', 'Project/Assets/Media Reel/Video Reel/js');

});


// The start task runs: scripts, sass and watch
gulp.task('start-ios', ['scripts-ios', 'sass-ios', 'watch-ios']);
gulp.task('start-android', ['scripts-android', 'sass-android', 'watch-android']);
gulp.task('start-windows', ['scripts-windows', 'sass-windows', 'watch-windows']);
gulp.task('start-jumpstart', ['scripts-jumpstart', 'sass-jumpstart', 'watch-jumpstart']);
gulp.task('start-leaderboard', ['scripts-leaderboard', 'sass-leaderboard', 'watch-leaderboard']);
gulp.task('start-photoReel', ['scripts-photoReel', 'sass-photoReel', 'watch-photoReel']);
gulp.task('start-videoReel', ['scripts-videoReel', 'sass-videoReel', 'watch-videoReel']);


// Watch for changes to js and scss files - iOS
gulp.task('watch-ios', function () {

    //spins up a server used to resync browser when there are changes to html, css, and js
    browserSync.init({
        server: rootDir.ios
    });

    // pre-process sass and js files if any matching files are changed
    gulp.watch([rootDir.ios + '/sass/**/*.scss'], ['sass-ios']);
    gulp.watch([rootDir.ios + '/js/**/*.js', '!' + rootDir.ios + '/js/**/app.js'], ['scripts-ios']);

    //browser sync watcher
    gulp.watch(rootDir.ios + '/*.html').on('change', browserSync.reload);
});


// Watch for changes to js and scss files - android
gulp.task('watch-android', function () {

    //spins up a server used to resync browser when there are changes to html, css, and js
    browserSync.init({
        server: rootDir.android
    });

    // pre-process sass and js files if any matching files are changed
    gulp.watch([rootDir.android + '/sass/**/*.scss'], ['sass-android']);
    gulp.watch([rootDir.android + '/js/**/*.js', '!' + rootDir.android + '/js/**/app.js'], ['scripts-android']);

    //browser sync watcher
    gulp.watch(rootDir.android + '/*.html').on('change', browserSync.reload);
});


// Watch for changes to js and scss files - windows
gulp.task('watch-windows', function () {

    //spins up a server used to resync browser when there are changes to html, css, and js
    browserSync.init({
        server: rootDir.windows
    });

    // pre-process sass and js files if any matching files are changed
    gulp.watch([rootDir.windows + '/sass/**/*.scss'], ['sass-windows']);
    gulp.watch([rootDir.windows + '/js/**/*.js', '!' + rootDir.windows + '/js/**/app.js'], ['scripts-windows']);

    //browser sync watcher
    gulp.watch(rootDir.windows + '/*.html').on('change', browserSync.reload);
});


// Watch for changes to js and scss files - jumpstart
gulp.task('watch-jumpstart', function () {

    //spins up a server used to resync browser when there are changes to html, css, and js
    browserSync.init({
        server: rootDir.jumpstart
    });

    // pre-process sass and js files if any matching files are changed
    gulp.watch([rootDir.jumpstart + '/sass/**/*.scss'], ['sass-jumpstart']);
    gulp.watch([rootDir.jumpstart + '/js/**/*.js', '!' + rootDir.jumpstart + '/js/**/app.js'], ['scripts-jumpstart']);

    //browser sync watcher
    gulp.watch(rootDir.jumpstart + '/*.html').on('change', browserSync.reload);
});


// Watch for changes to js and scss files - leaderboard
gulp.task('watch-leaderboard', function () {

    //spins up a server used to resync browser when there are changes to html, css, and js
    browserSync.init({
        server: rootDir.leaderboard
    });

    // pre-process sass and js files if any matching files are changed
    gulp.watch([rootDir.leaderboard + '/sass/**/*.scss'], ['sass-leaderboard']);
    gulp.watch([rootDir.leaderboard + '/js/**/*.js', '!' + rootDir.leaderboard + '/js/**/app.js'], ['scripts-leaderboard']);

    //browser sync watcher
    gulp.watch(rootDir.leaderboard + '/*.html').on('change', browserSync.reload);
});


// Watch for changes to js and scss files - photo reel
gulp.task('watch-photoReel', function () {

    //spins up a server used to resync browser when there are changes to html, css, and js
    browserSync.init({
        server: rootDir.photoReel
    });

    // pre-process sass and js files if any matching files are changed
    gulp.watch([rootDir.photoReel + '/sass/**/*.scss'], ['sass-photoReel']);
    gulp.watch([rootDir.photoReel + '/js/**/*.js', '!' + rootDir.photoReel + '/js/**/app.js'], ['scripts-photoReel']);

    //browser sync watcher
    gulp.watch(rootDir.photoReel + '/*.html').on('change', browserSync.reload);
});


// Watch for changes to js and scss files - video reel
gulp.task('watch-videoReel', function () {

    //spins up a server used to resync browser when there are changes to html, css, and js
    browserSync.init({
        server: rootDir.videoReel
    });

    // pre-process sass and js files if any matching files are changed
    gulp.watch([rootDir.videoReel + '/sass/**/*.scss'], ['sass-videoReel']);
    gulp.watch([rootDir.videoReel + '/js/**/*.js', '!' + rootDir.videoReel + '/js/**/app.js'], ['scripts-videoReel']);

    //browser sync watcher
    gulp.watch(rootDir.videoReel + '/*.html').on('change', browserSync.reload);
});


// Compile sass files using Autoprefixer - iOS
gulp.task('sass-ios', function () {
    watchSass(rootDir.ios);
});

// Compile sass files using Autoprefixer - android
gulp.task('sass-android', function () {
    watchSass(rootDir.android);
});

// Compile sass files using Autoprefixer - windows
gulp.task('sass-windows', function () {
    watchSass(rootDir.windows);
});

// Compile sass files using Autoprefixer - jumpstart
gulp.task('sass-jumpstart', function () {
    watchSass(rootDir.jumpstart);
});

// Compile sass files using Autoprefixer - leaderboard
gulp.task('sass-leaderboard', function () {
    watchSass(rootDir.leaderboard);
});

// Compile sass files using Autoprefixer - photo reel
gulp.task('sass-photoReel', function () {
    watchSass(rootDir.photoReel);
});

// Compile sass files using Autoprefixer - video reel
gulp.task('sass-videoReel', function () {
    watchSass(rootDir.videoReel);
});


// concatenate js scripts - iOS
gulp.task('scripts-ios', function () {

    scripts(scriptFiles.ios, rootDir.ios);

});

// concatenate js scripts - android
gulp.task('scripts-android', function () {

    scripts(scriptFiles.android, rootDir.android);

});

// concatenate js scripts - windows
gulp.task('scripts-windows', function () {

    scripts(scriptFiles.windows, rootDir.windows);

});

// concatenate js scripts - jumpstart
gulp.task('scripts-jumpstart', function () {

    scripts(scriptFiles.jumpstart, rootDir.jumpstart);

});

// concatenate js scripts - leaderboard
gulp.task('scripts-leaderboard', function () {

    scripts(scriptFiles.leaderboard, rootDir.leaderboard);

});

// concatenate js scripts - photo reel
gulp.task('scripts-photoReel', function () {

    scripts(scriptFiles.photoReel, rootDir.photoReel);

});

// concatenate js scripts - video reel
gulp.task('scripts-videoReel', function () {

    scripts(scriptFiles.videoReel, rootDir.videoReel);

});


// clear the build folder - iOS
gulp.task('clean-ios', function () {

    return gulp.src('Project/Assets/Devices/iOS/build', {read: false})
        .pipe(clean());

});

// clear the build folder - android
gulp.task('clean-android', function () {

    return gulp.src('Project/Assets/Devices/Android/build', {read: false})
        .pipe(clean());

});

// clear the build folder - windows
gulp.task('clean-windows', function () {

    return gulp.src('Project/Assets/Devices/Windows/build', {read: false})
        .pipe(clean());

});

// clear the build folder - jumpstart
gulp.task('clean-jumpstart', function () {

    return gulp.src('Project/Assets/Jumpstart/build', {read: false})
        .pipe(clean());

});


// clear the build folder - leaderboard
gulp.task('clean-leaderboard', function () {

    return gulp.src('Project/Assets/Media Reel/Leaderboard/build', {read: false})
        .pipe(clean());

});

// clear the build folder - photo reel
gulp.task('clean-photoReel', function () {

    return gulp.src('Project/Assets/Media Reel/Photo Reel/build', {read: false})
        .pipe(clean());

});

// clear the build folder - video reel
gulp.task('clean-videoReel', function () {

    return gulp.src('Project/Assets/Media Reel/Video Reel/build', {read: false})
        .pipe(clean());

});


// build for ios deployment
gulp.task('build-ios', ['scripts-ios', 'sass-ios', 'clean-ios'], function () {
    build(paths_ios, rootDir.ios);
});
// build for android deployment
gulp.task('build-android', ['scripts-android', 'sass-android', 'clean-android'], function () {
    build(paths_android, rootDir.android);
});
// build for windows
gulp.task('build-windows', ['scripts-windows', 'sass-windows', 'clean-windows'], function () {
    build(paths_windows, rootDir.windows);
});
// build for jumpstart
gulp.task('build-jumpstart', ['scripts-jumpstart', 'sass-jumpstart', 'clean-jumpstart'], function () {
    build(paths_jumpstart, rootDir.jumpstart);
});
// build for leaderboard
gulp.task('build-leaderboard', ['scripts-leaderboard', 'sass-leaderboard', 'clean-leaderboard'], function () {
    build(paths_leaderboard, rootDir.leaderboard);
});
// build for photo reel
gulp.task('build-photoReel', ['scripts-photoReel', 'sass-photoReel', 'clean-photoReel'], function () {
    build(paths_photoReel, rootDir.photoReel);
});
// build for video reel
gulp.task('build-videoReel', ['scripts-videoReel', 'sass-videoReel', 'clean-videoReel'], function () {
    build(paths_videoReel, rootDir.videoReel);
});


//create css and js files and place them in a folder
function createFile(fileName, content, folder) {

    var stream = source(fileName);
    stream.end(content);
    stream.pipe(gulp.dest(folder));

}


function watchSass(dir) {

    var appscss;
    if (dir === rootDir.windows) {
        appscss = 'mzWinSA.scss';
    } else if (dir === rootDir.ios) {
        appscss = 'app-ios.scss';
    } else if (dir === rootDir.android) {
        appscss = 'app-android.scss';
    } else if (dir === rootDir.jumpstart) {
        appscss = 'app-jumpstart.scss';
    } else if (dir === rootDir.leaderboard) {
        appscss = 'app-leaderboard.scss';
    } else if (dir === rootDir.photoReel) {
        appscss = 'app-photoreel.scss';
    } else if (dir === rootDir.videoReel) {
        appscss = 'app-videoreel.scss';
    } else {
        appscss = 'app.scss';
    }

    // this will allow for errors to be thrown without
    // totally stopping the watcher task
    var onError = function (err) {
        notify.onError({
            title: "Gulp",
            subtitle: "Failure!",
            message: "Error: <%= error.message %>",
            sound: "Beep"
        })(err);
        this.emit('end');
    };

    return gulp.src([dir + '/sass/' + appscss])
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 3 versions', 'iOS 6'],
            cascade: false
        }))
        .pipe(cleanCSS())
        .pipe(gulp.dest(dir + '/css'))
        .pipe(browserSync.stream());

}


function scripts(myScripts, dir) {

    // var appjs = 'app.js'

    // if (dir === rootDir.windows) {
    //     appjs = 'mzWinSA.js';
    // }

    var appjs;
    if (dir === rootDir.windows) {
        appjs = 'mzWinSA.js';
    } else if (dir === rootDir.ios) {
        appjs = 'app-ios.js';
    } else if (dir === rootDir.android) {
        appjs = 'app-android.js';
    } else if (dir === rootDir.jumpstart) {
        appjs = 'app-jumpstart.js';
    } else if (dir === rootDir.leaderboard) {
        appjs = 'app-leaderboard.js';
    } else if (dir === rootDir.photoReel) {
        appjs = 'app-photoreel.js';
    } else if (dir === rootDir.videoReel) {
        appjs = 'app-videoreel.js';
    } else {
        appjs = 'app.js';
    }

    gulp.src(myScripts)
        .pipe(concat(appjs))
        .pipe(uglify())
        .pipe(gulp.dest(dir + '/js'))
        .pipe(browserSync.stream());

}


function build(path, dir) {

    // var appjs = 'app.js';
    //
    // if (dir === rootDir.windows) {
    //     appjs = 'mzWinSA.js';
    // }

    var timer = setTimeout(function() {

        clearTimeout(timer);

        var appjs;
        if (dir === rootDir.windows) {
            appjs = 'mzWinSA.js';
        } else if (dir === rootDir.ios) {
            appjs = 'app-ios.js';
        } else if (dir === rootDir.android) {
            appjs = 'app-android.js';
        } else if (dir === rootDir.jumpstart) {
            appjs = 'app-jumpstart.js';
        } else if (dir === rootDir.leaderboard) {
            appjs = 'app-leaderboard.js';
        } else if (dir === rootDir.photoReel) {
            appjs = 'app-photoreel.js';
        } else if (dir === rootDir.videoReel) {
            appjs = 'app-videoreel.js';
        } else {
            appjs = 'app.js';
        }

        gulp.src([
            dir + '/js/' + appjs, dir + '/js/mz/**', dir + '/js/vendor/**'])
            .pipe(replace('../img/', ''))
            .pipe(gulp.dest(dir + '/build'));

        // Copy CSS
        gulp.src(path['css'])
            .pipe(replace('../img/', ''))
            .pipe(replace('../fonts/', ''))
            // .pipe(replace(/[\w\.\$]+(?=png|jpg|gif|svg)\w+/g, function(match) {
            .pipe(replace(/url\(([^)]+)\)/g, function(match) {

                var getFirst = match.slice(0,4);
                var getMiddle = match.slice(4,-1);
                var getLast = match.slice(match.length-1,match.length);

                return getFirst + "'" + getMiddle + "'" + getLast;
            }))
            .pipe(gulp.dest(dir + '/build'));

        // copy html files for pages
        gulp.src(path['pages'])
            .pipe(replace('img/', ''))
            .pipe(replace('css/', ''))
            .pipe(replace(/src="js\/(.+\/)?(.+)(.js)">/g, 'src="$2$3">'))
            .pipe(gulp.dest(dir + '/build'));

        // copy fonts
        gulp.src([dir + '/fonts/**/*'])
            .pipe(gulp.dest(dir + '/build'));

        // Take all images from the img folder and all
        // subfolders and merge into the same root folder
        gulp.src([dir + '/img/**/*.{jpg,gif,png,svg}'])
            .pipe(flatten())
            .pipe(gulp.dest(dir + '/build'));

    }, 1000);

}


//COMPRESS IMAGES - REMOVES WHITE SPACE MAINLY. MOST IMAGES SHOULD BE MOSTLY OPTIMIZED BUT THIS TASK WILL SAVE SOME ADDITIONAL KBS
gulp.task('compress-image-ios', function () {
    compressImages(rootDir.ios);
});
gulp.task('compress-image-android', function () {
    compressImages(rootDir.android);
});
gulp.task('compress-image-windows', function () {
    compressImages(rootDir.windows);
});
gulp.task('compress-image-jumpstart', function () {
    compressImages(rootDir.jumpstart);
});
gulp.task('compress-image-leaderboard', function () {
    compressImages(rootDir.leaderboard);
});

gulp.task('compress-image-photoReel', function () {
    compressImages(rootDir.photoReel);
});

gulp.task('compress-image-videoReel', function () {
    compressImages(rootDir.videoReel);
});


function compressImages(dir) {
    gulp.src([dir + '/img/*'])
        .pipe(imagemin(
            {
                interlaced: true,
                progressive: true,
                optimizationLevel: 5,
                svgoPlugins: [{removeViewBox: true}]
            }
        ))
        .pipe(gulp.dest(dir + '/img'))
}


// Lint you Javascript
gulp.task('lint-ios', function () {
    lint(rootDir.ios);
});
gulp.task('lint-android', function () {
    lint(rootDir.android);
});
gulp.task('lint-windows', function () {
    lint(rootDir.windows);
});
gulp.task('lint-jumpstart', function () {
    lint(rootDir.jumpstart);
});
gulp.task('lint-leaderboard', function () {
    lint(rootDir.leaderboard);
});
gulp.task('lint-photoReel', function () {
    lint(rootDir.photoReel);
});
gulp.task('lint-videoReel', function () {
    lint(rootDir.videoReel);
});

function lint(dir) {
    return gulp.src(dir + '/js/app.js')
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'));
}


// Copy html template into ios directory and rename
// gulp.task('rename-file', function () {
//
//     gulp.src("template.html")
//         .pipe(rename("index.html"))
//         .pipe(gulp.dest("Project/Assets/Devices/iOS"));
//
// });


//CREATE HTML STARTER FILE FOR IOS - takes template from one directory and copies it to another directory
// gulp.task('html-temp', function () {
//     return gulp.src('template.html')
//     // .pipe(template('template.html'))
//         .pipe(gulp.dest('Project/Assets/Devices/iOS'));
// });


//ADD JS AND CSS LIBRARIES TO IOS PROJECT
gulp.task('add-lib-ios', function () {

    download('https://assets.smartactivator.com/js/mz/MZ.2.2.0.4.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/mz'));

    download('https://assets.smartactivator.com/js/mz/OS.1.0.3.1.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/mz'));

});


// rename via string
// gulp.src("./src/main/text/hello.txt")
//     .pipe(rename("main/text/ciao/goodbye.md"))
//     .pipe(gulp.dest("./dist")); // ./dist/main/text/ciao/goodbye.md


//ADD JS AND CSS LIBRARIES TO WINDOWS PROJECT
gulp.task('add-lib-win', function () {

    download('https://assets.smartactivator.com/js/mz/MZ.2.2.0.4.min.js')
        .pipe(rename("MZ.js"))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/mz'));

    download('https://assets.smartactivator.com/js/mz/OS.1.0.3.1.min.js')
        .pipe(rename("OS.js"))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/mz'));

});


//ADD JS AND CSS LIBRARIES TO ANDROID PROJECT
gulp.task('add-lib-android', function () {

    download('https://assets.smartactivator.com/js/mz/MZ.2.2.0.4.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/mz'));

    download('https://assets.smartactivator.com/js/mz/OS.1.0.3.1.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/mz'));


});


//ADD JS AND CSS LIBRARIES TO JUMPSTART
gulp.task('add-lib-jumpstart', function () {

    download('https://assets.smartactivator.com/js/mz/MZ.2.2.0.4.min.js')
        .pipe(gulp.dest('Project/Assets/Jumpstart/js/mz'));

    download('https://assets.smartactivator.com/js/mz/OS.1.0.3.1.min.js')
        .pipe(gulp.dest('Project/Assets/Jumpstart/js/mz'));

});


//DOWNLOAD JQUERY
gulp.task('lib-jquery-ios', function () {

    download('https://assets.smartactivator.com/js/gen/jquery/2.2.3/jquery-2.2.3.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/jquery-2.2.3.min.js"></script>');

});


gulp.task('lib-jquery-android', function () {

    download('https://assets.smartactivator.com/js/gen/jquery/2.2.3/jquery-2.2.3.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/jquery-2.2.3.min.js"></script>');

});


gulp.task('lib-jquery-windows', function () {

    download('https://assets.smartactivator.com/js/gen/jquery/2.2.3/jquery-2.2.3.min.js')
        .pipe(rename("jquery.js"))
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/jquery-2.2.3.min.js"></script>');

});


gulp.task('lib-jquery-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/gen/jquery/2.2.3/jquery-2.2.3.min.js"></script>');

});


//DOWNLOAD VELOCITY
gulp.task('lib-velocity-ios', function () {

    download('https://assets.smartactivator.com/js/graphics/velocity/1.2.3/velocity.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    download('https://assets.smartactivator.com/js/graphics/velocity/1.2.3/velocity.ui.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/velocity.min.js"></script>');
    console.log('<script src="js/vendor/velocity.ui.min.js"></script>');

});


gulp.task('lib-velocity-android', function () {

    download('https://assets.smartactivator.com/js/graphics/velocity/1.2.3/velocity.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    download('https://assets.smartactivator.com/js/graphics/velocity/1.2.3/velocity.ui.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/velocity.min.js"></script>');
    console.log('<script src="js/vendor/velocity.ui.min.js"></script>');

});


gulp.task('lib-velocity-windows', function () {

    download('https://assets.smartactivator.com/js/graphics/velocity/1.2.3/velocity.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    download('https://assets.smartactivator.com/js/graphics/velocity/1.2.3/velocity.ui.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/velocity.min.js"></script>');
    console.log('<script src="js/vendor/velocity.ui.min.js"></script>');

});

gulp.task('lib-velocity-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/graphics/velocity/1.2.3/velocity.min.js"></script>');
    console.log('<script src="https://assets.smartactivator.com/js/graphics/velocity/1.2.3/velocity.ui.min.js"></script>');

});


//DOWNLOAD VIDEOJS
gulp.task('lib-videojs-ios', function () {

    download('https://assets.smartactivator.com/js/graphics/videojs/5.16.0/video.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    download('https://assets.smartactivator.com/js/graphics/videojs/5.16.0/video-js.css')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/css'));

    console.log('<script src="js/vendor/video.min.js"></script>');
    console.log('<link rel="stylesheet" type="text/css" href="css/video-js.css">');

});


gulp.task('lib-videojs-android', function () {

    download('https://assets.smartactivator.com/js/graphics/videojs/5.16.0/video.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    download('https://assets.smartactivator.com/js/graphics/videojs/5.16.0/video-js.css')
        .pipe(gulp.dest('Project/Assets/Devices/Android/css'));

    console.log('<script src="js/vendor/video.min.js"></script>');
    console.log('<link rel="stylesheet" type="text/css" href="css/video-js.css">');

});


gulp.task('lib-videojs-windows', function () {

    download('https://assets.smartactivator.com/js/graphics/videojs/5.16.0/video.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    download('https://assets.smartactivator.com/js/graphics/videojs/5.16.0/video-js.css')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/css'));

    console.log('<script src="js/vendor/video.min.js"></script>');
    console.log('<link rel="stylesheet" type="text/css" href="css/video-js.css">');

});


gulp.task('lib-videojs-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/graphics/videojs/5.16.0/video.min.js"></script>');
    console.log('<link rel="stylesheet" type="text/css" href="https://assets.smartactivator.com/js/graphics/videojs/5.16.0/video-js.css">');

});


//DOWNLOAD ANGULAR JS
gulp.task('lib-angular-ios', function () {

    download('https://assets.smartactivator.com/js/gen/angular/1.6.2/angular.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/angular.min.js"></script>');

});

gulp.task('lib-angular-android', function () {

    download('https://assets.smartactivator.com/js/gen/angular/1.6.2/angular.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/angular.min.js"></script>');

});

gulp.task('lib-angular-windows', function () {

    download('https://assets.smartactivator.com/js/gen/angular/1.6.2/angular.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/angular.min.js"></script>');

});

gulp.task('lib-angular-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/gen/angular/1.6.2/angular.min.js"></script>');

});


//DOWNLOAD EASEL
gulp.task('lib-easel-ios', function () {

    download('https://assets.smartactivator.com/js/graphics/easeljs/0.8.32/easeljs-0.8.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/easeljs-0.8.2.min"></script>');

});

gulp.task('lib-easel-android', function () {

    download('https://assets.smartactivator.com/js/graphics/easeljs/0.8.32/easeljs-0.8.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/easeljs-0.8.2.min"></script>');

});

gulp.task('lib-easel-windows', function () {

    download('https://assets.smartactivator.com/js/graphics/easeljs/0.8.32/easeljs-0.8.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/easeljs-0.8.2.min"></script>');

});

gulp.task('lib-easel-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/graphics/easeljs/0.8.32/easeljs-0.8.2.min.js"></script>');

});


//DOWNLOAD TWEEN
gulp.task('lib-tween-ios', function () {

    download('https://assets.smartactivator.com/js/graphics/tweenjs/0.6.2/tweenjs-0.6.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/tweenjs-0.6.2.min.js"></script>');

});

gulp.task('lib-tween-android', function () {

    download('https://assets.smartactivator.com/js/graphics/tweenjs/0.6.2/tweenjs-0.6.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/tweenjs-0.6.2.min.js"></script>');

});

gulp.task('lib-tween-windows', function () {

    download('https://assets.smartactivator.com/js/graphics/tweenjs/0.6.2/tweenjs-0.6.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/tweenjs-0.6.2.min.js"></script>');

});

gulp.task('lib-tween-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/graphics/tweenjs/0.6.2/tweenjs-0.6.2.min.js"></script>');

});


//DOWNLOAD PRELOAD
gulp.task('lib-preload-ios', function () {

    download('https://assets.smartactivator.com/js/graphics/preloadjs/0.6.2/preloadjs-0.6.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/preloadjs-0.6.2.min.js"></script>');

});

gulp.task('lib-preload-android', function () {

    download('https://assets.smartactivator.com/js/graphics/preloadjs/0.6.2/preloadjs-0.6.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/preloadjs-0.6.2.min.js"></script>');

});

gulp.task('lib-preload-windows', function () {

    download('https://assets.smartactivator.com/js/graphics/preloadjs/0.6.2/preloadjs-0.6.2.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/preloadjs-0.6.2.min.js"></script>');

});


gulp.task('lib-preload-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/graphics/preloadjs/0.6.2/preloadjs-0.6.2.min.js"></script>');

});


//DOWNLOAD BlueImp LoadImage
gulp.task('lib-BlueImp-ios', function () {

    download('https://assets.smartactivator.com/js/graphics/blueimp-load-image/2.11.0/load-image.all.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/load-image.all.min.js"></script>');

});

gulp.task('lib-BlueImp-android', function () {

    download('https://assets.smartactivator.com/js/graphics/blueimp-load-image/2.11.0/load-image.all.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/load-image.all.min.js"></script>');

});

gulp.task('lib-BlueImp-windows', function () {

    download('https://assets.smartactivator.com/js/graphics/blueimp-load-image/2.11.0/load-image.all.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/load-image.all.min.js"></script>');

});


gulp.task('lib-BlueImp-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/graphics/blueimp-load-image/2.11.0/load-image.all.min.js"></script>');

});


//DOWNLOAD Hammer
gulp.task('lib-hammer-ios', function () {

    download('https://assets.smartactivator.com/js/gen/hammer/2.0.8/hammer-2.0.8.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/hammer-2.0.8.min.js"></script>');

});

gulp.task('lib-hammer-android', function () {

    download('https://assets.smartactivator.com/js/gen/hammer/2.0.8/hammer-2.0.8.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/hammer-2.0.8.min.js"></script>');

});

gulp.task('lib-hammer-windows', function () {

    download('https://assets.smartactivator.com/js/gen/hammer/2.0.8/hammer-2.0.8.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/hammer-2.0.8.min.js"></script>');

});

gulp.task('lib-hammer-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/gen/hammer/2.0.8/hammer-2.0.8.min.js"></script>');

});


//DOWNLOAD Blast
gulp.task('lib-blast-ios', function () {

    download('https://assets.smartactivator.com/js/gen/blast/2.0.0/jquery.blast.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/jquery.blast.min.js"></script>');

});

gulp.task('lib-blast-android', function () {

    download('https://assets.smartactivator.com/js/gen/blast/2.0.0/jquery.blast.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/jquery.blast.min.js"></script>');

});

gulp.task('lib-blast-windows', function () {

    download('https://assets.smartactivator.com/js/gen/blast/2.0.0/jquery.blast.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/jquery.blast.min.js"></script>');

});


gulp.task('lib-blast-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/gen/blast/2.0.0/jquery.blast.min.js"></script>');

});


//DOWNLOAD QR Code
gulp.task('lib-qrcode-ios', function () {

    download('https://assets.smartactivator.com/js/gen/qrcode/qrcode.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/qrcode.min.js"></script>');

});

gulp.task('lib-qrcode-android', function () {

    download('https://assets.smartactivator.com/js/gen/qrcode/qrcode.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/qrcode.min.js"></script>');

});

gulp.task('lib-qrcode-windows', function () {

    download('https://assets.smartactivator.com/js/gen/qrcode/qrcode.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/qrcode.min.js"></script>');

});


gulp.task('lib-qrcode-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/gen/qrcode/qrcode.min.js"></script>');

});


//DOWNLOAD Restive
gulp.task('lib-restive-ios', function () {

    download('https://assets.smartactivator.com/js/graphics/restive/1.3.7/restive.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/restive.min.js"></script>');

});

gulp.task('lib-restive-android', function () {

    download('https://assets.smartactivator.com/js/graphics/restive/1.3.7/restive.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/restive.min.js"></script>');

});

gulp.task('lib-restive-windows', function () {

    download('https://assets.smartactivator.com/js/graphics/restive/1.3.7/restive.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/restive.min.js"></script>');

});


gulp.task('lib-restive-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/graphics/restive/1.3.7/restive.min.js"></script>');

});


//DOWNLOAD Turn
gulp.task('lib-turn-ios', function () {

    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));
    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.html4.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));
    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.css')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/css'));

    console.log('<script src="js/vendor/turn.min.js"></script>');
    console.log('<script src="js/vendor/turn.html4.min.js"></script>');
    console.log('<link rel="stylesheet" type="text/css" href="css/turn.css">');

});

gulp.task('lib-turn-android', function () {

    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));
    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.html4.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));
    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.css')
        .pipe(gulp.dest('Project/Assets/Devices/Android/css'));

    console.log('<script src="js/vendor/turn.min.js"></script>');
    console.log('<script src="js/vendor/turn.html4.min.js"></script>');
    console.log('<link rel="stylesheet" type="text/css" href="css/turn.css">');

});

gulp.task('lib-turn-windows', function () {

    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));
    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.html4.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));
    download('https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.css')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/css'));

    console.log('<script src="js/vendor/turn.min.js"></script>');
    console.log('<script src="js/vendor/turn.html4.min.js"></script>');
    console.log('<link rel="stylesheet" type="text/css" href="css/turn.css">');

});


gulp.task('lib-turn-jumpstart', function () {

    console.log('<script src="https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.min.js"></script>');
    console.log('<script src="https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.html4.min.js"></script>');
    console.log('<link rel="stylesheet" type="text/css" href="https://assets.smartactivator.com/js/graphics/turn/4.1.0/turn.css">');

});


//DOWNLOAD Modernizer
gulp.task('lib-modernizer-ios', function () {

    download('https://assets.smartactivator.com/js/gen/modernizr/2.5.3/modernizr.2.5.3.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/js/vendor'));

    console.log('<script src="js/vendor/modernizr.2.5.3.min.js"></script>');

});

gulp.task('lib-modernizer-android', function () {

    download('https://assets.smartactivator.com/js/gen/modernizr/2.5.3/modernizr.2.5.3.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Android/js/vendor'));

    console.log('<script src="js/vendor/modernizr.2.5.3.min.js"></script>');

});

gulp.task('lib-modernizer-windows', function () {

    download('https://assets.smartactivator.com/js/gen/modernizr/2.5.3/modernizr.2.5.3.min.js')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/js/vendor'));

    console.log('<script src="js/vendor/modernizr.2.5.3.min.js"></script>');

});

gulp.task('lib-modernizer-jumpstart', function () {


    console.log('<script src="https://assets.smartactivator.com/js/gen/modernizr/2.5.3/modernizr.2.5.3.min.js"></script>');

});


//DOWNLOAD Animate.css
gulp.task('lib-animate-ios', function () {

    download('https://raw.github.com/daneden/animate.css/master/animate.css')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/css'));

});

gulp.task('lib-animate-android', function () {

    download('https://raw.github.com/daneden/animate.css/master/animate.css')
        .pipe(gulp.dest('Project/Assets/Devices/Android/css'));

});

gulp.task('lib-animate-windows', function () {

    download('https://raw.github.com/daneden/animate.css/master/animate.css')
        .pipe(gulp.dest('Project/Assets/Devices/Windows/css'));

});

gulp.task('lib-animate-jumpstart', function () {

    console.log('<link rel="stylesheet" type="text/css" href="https://raw.github.com/daneden/animate.css/master/animate.css">');

});


//Modules
gulp.task('module-motion-photo-ios', function () {

    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/main.css')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/overlay-template.html')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/gif.worker.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/gif.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/mzmotion.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/mzmp-recipe-overlay-template.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/mzmp-config-overlay-template.js')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/gif.js.map')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/gif.worker.js.map')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/modules/MotionPhoto'));
    download('http://smartactivator.blob.core.windows.net/frontend/gulp/templates/smartactivator/ios/motionphoto/1.0/spinner.png')
        .pipe(gulp.dest('Project/Assets/Devices/iOS/Modules/MotionPhoto'));

});


